<?php
    session_start();
    include 'db.php';
    header( "Content-type: application/json");
    
    
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    

    switch ( $_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $itemID = $_REQUEST['ID'] + 0;
            $bidResults= selectDb("SELECT BTime, BPrice FROM BID WHERE itemID='$itemID' ORDER BY BPrice DESC");
            
            if ( ! $bidResults ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $bidList = array();
                while( $record = mysqli_fetch_assoc( $bidResults ) ) {
                    $bidList[] = $record;
                }
                mysqli_free_result( $bidResults );
                echo json_encode( $bidList );
            }
            break;
        
        case 'POST':
            if ( ! $_SESSION['loggedin'] ) {
                http_response_code( 401 );
                exit(1);
            }
            $safe_description = mysqli_real_escape_string( $link, $_REQUEST["Description"] );
            $quantity = $_REQUEST["Quantity"] + 0;
            if ( strlen( $safe_description ) <= 0 ||
                 strlen( $safe_description ) > 80 ||
                 $quantity <= 0 ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }

            $query = "INSERT INTO shoppinglist ( Quantity, Description ) VALUES ( $quantity, '$safe_description' )";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            $record["ID"] = mysqli_insert_id( $link );
            echo json_encode( $record );
            break;
        
        case 'DELETE':
            if ( ! $_SESSION['loggedin'] ) {
                http_response_code( 401 );
                exit(1);
            }
            $ID = $_REQUEST['ID'] + 0;
            $query = "DELETE FROM shoppinglist WHERE ID=$ID";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;

    }
