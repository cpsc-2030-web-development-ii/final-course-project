# Open Market Project

The Open Market website is designed to provide a place where sellers and buyers can freely negotiate.
The project was developed from scratch during CPSC 2030 - Web Development II as a course requirement, putting together all technologies taught.
In the very beginning, the first database search fetches the item with the lowest price and shows its value on the top left of the page with the announcement "Starting at.." which also links to the correspondent item in the cards section.
Next, a query retrieves from the MySQL database the last three registered items showing them in a carousel slide.
The photos showed in each carousel slide links the correspondent item card listed at the bottom of the page.

## Web Layout

![alt text](img/OpenMarketWeb1.png "Title Text")

## Responsive Layout (iPad)

![alt text](img/OpenMarketWebIpad.png "Title Text")

## Responsive Layout (iPhone)

![alt text](img/OpenMarketWebiPhone.png "Title Text")

## Project Design

![alt text](img/open_market.png "Title Text")

## Database Mapping

![alt text](img/DatabaseMapping.png "Title Text")

## Database Structure

![alt text](img/DbDiagram.png "Title Text")

## Installing

### 0. Prerequisites

- XAMPP
- MySQL
- Git

In order to run this project locally, you'll need `XAMPP` and `MySQL`.
To install `MySQL` follow the instructions in the
[Apache website](https://www.apachefriends.org/index.html).

To install `XAMPP` follow the instructions in the
[MySQL website](https://www.mysql.com/).

Git is a version control system (VCS). To install `git` follow
the instructions in the [Git website](https://git-scm.com/downloads)

### 1. Clone

The first step is to clone the project:

```
git clone https://gitlab.com/cpsc-2030-web-development-ii/final-course-project.git
```

Enter the project directory:

```
cd final-course-project
```

### 2. Create the MySQL database and instantiate it with the following script

```
-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 20, 2021 at 04:28 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Auction`
--

-- --------------------------------------------------------

--
-- Table structure for table `BID`
--

CREATE TABLE `BID` (
  `BidID` int(11) NOT NULL,
  `BTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BPrice` decimal(5,0) NOT NULL,
  `MemberID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `BID`
--

INSERT INTO `BID` (`BidID`, `BTime`, `BPrice`, `MemberID`, `itemID`) VALUES
(1, '2021-08-07 02:20:30', '91', 8, 1),
(2, '2021-08-07 02:19:21', '383', 12, 6),
(3, '2021-08-07 02:19:30', '399', 14, 9),
(4, '2021-06-21 12:04:00', '99', 11, 10),
(5, '2021-07-21 09:00:00', '88', 8, 1),
(6, '2021-08-07 02:19:46', '385', 12, 6),
(7, '2021-07-21 11:01:14', '388', 14, 9),
(8, '2021-08-03 05:23:36', '101', 11, 10),
(9, '2021-05-12 14:11:00', '233', 13, 8),
(10, '2021-03-21 18:11:00', '375', 6, 7),
(11, '2021-07-31 14:11:14', '96', 15, 11),
(12, '2021-08-01 13:54:13', '195', 13, 12),
(13, '2021-08-01 13:54:13', '236', 4, 13);

-- --------------------------------------------------------

--
-- Table structure for table `ITEM`
--

CREATE TABLE `ITEM` (
  `itemID` int(11) NOT NULL,
  `itemTitle` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `startDt` date NOT NULL,
  `endDt` date NOT NULL,
  `startBid` decimal(5,0) NOT NULL,
  `bidIncrement` decimal(5,0) NOT NULL,
  `PhotoPath` varchar(255) NOT NULL,
  `MemberID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ITEM`
--

INSERT INTO `ITEM` (`itemID`, `itemTitle`, `description`, `startDt`, `endDt`, `startBid`, `bidIncrement`, `PhotoPath`, `MemberID`) VALUES
(1, 'New bike used couple times', 'bought brand new only used it a few times I have reciept if you want', '2021-01-19', '2021-11-15', '85', '1', 'https://unsplash.com/photos/0ClfreiNppM?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22RF3-UyS_V5I%22%7D%7D', 1),
(2, 'Couch Sofa', 'Couch Sofa + PULL OUT BED (conversion Queen size) + plus storage', '2021-01-08', '2021-12-28', '185', '1', 'https://unsplash.com/photos/2dcYhvbHV-M?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22DwCCWPTsFF8%22%7D%7D', 2),
(3, 'Office chair', 'I have 5 of these amazing quality, ergonomic chairs. Purchased for over $900 each', '2021-01-21', '2021-10-30', '485', '1', 'https://unsplash.com/photos/9l_326FISzk?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22TrMEfNqww7s%22%7D%7D', 2),
(4, 'Bar Fridge', 'Used mini fridge, approx 2ft by 3ft', '2021-02-12', '2021-12-31', '245', '1', 'https://unsplash.com/photos/3h7jbWXhpgM?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22M3lbeTMnA1M%22%7D%7D', 3),
(5, 'Carry on luggage', 'Like new, less used, lightweight', '2021-01-18', '2021-11-01', '72', '1', 'https://unsplash.com/photos/TVllFyGaLEA?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22bS6B33a94h0%22%7D%7D', 3),
(6, '15.6 Laptop (mint, barely used) i7, 2.3ghz, 16gb RAM, 1TB w/ Touch Screen', 'Like brand new; barely used. All keys and screen have been barely touched', '2021-03-11', '2021-11-30', '372', '1', 'https://unsplash.com/photos/RSCirJ70NDM?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22ZpU5oLzQKtw%22%7D%7D', 7),
(7, '65 smart TV', 'built in wifi and Ethernet, web browser, smart hub, comes with original box and remote', '2021-03-09', '2021-12-08', '624', '1', 'https://unsplash.com/photos/ngMtsE5r9eI?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22o2rPXee21cY%22%7D%7D', 9),
(8, 'Coffee machine', 'brand new only used it a few times', '2021-02-09', '2021-12-19', '220', '1', 'https://unsplash.com/photos/flqeR5XUNxY?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22EWgN5kc_9l8%22%7D%7D', 4),
(9, 'Sofa Bed', 'Queen size sofa bed with bottom storage space', '2021-06-13', '2021-11-17', '385', '1', 'https://unsplash.com/photos/Q2QhOxN5enk?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22IFcEhJqem0Q%22%7D%7D', 5),
(10, 'Office table', 'barely used, no straches, like new', '2021-04-22', '2021-10-10', '98', '1', 'https://unsplash.com/photos/_-KLkj7on_c?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%225RArX9WcBkk%22%7D%7D', 6),
(11, 'Horizontal Fridge', 'Only 2 years of use, approx 4ft by 3ft', '2021-04-04', '2021-12-01', '110', '1', 'https://unsplash.com/photos/MP0bgaS_d1c?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22DwCCWPTsFF8%22%7D%7D', 7),
(12, 'Carbon fiber carry on luggage', 'Used only once, black, lightweight', '2021-04-03', '2021-11-24', '472', '1', 'https://unsplash.com/photos/xFItahF3CY4?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%224HraACy4gPg%22%7D%7D', 6),
(13, '17 Laptop, i5, 2.1ghz, 32gb RAM, 2TB', 'Like brand new, barely used, fully working ', '2021-08-01', '2021-12-14', '472', '1', 'https://unsplash.com/photos/pSmD3L7z8hs?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22-ne6m-L6lDs%22%7D%7D', 7),
(14, '48 smart TV', 'built in wifi and Ethernet, web browser, hub, with original box and remote', '2021-07-09', '2021-09-30', '324', '1', 'https://www.pexels.com/photo/turned-on-flat-screen-smart-television-ahead-1444416/', 9),
(15, 'Wireless keyboard', 'built in bluetooth, rechargeable lithium ion battery, still in original box', '2021-07-09', '2021-09-30', '94', '1', 'https://unsplash.com/photos/Nl79ckrM_1Q?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22dh8labKNEgs%22%7D%7D', 12),
(16, 'Smartphone 4G', 'built in WI-FI, bluetooth, rechargeable lithium ion battery, smell like new', '2021-07-01', '2021-10-30', '194', '1', 'https://unsplash.com/photos/oCfkSnqZ0SI?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22180IDsy8yS4%22%7D%7D', 8),
(17, 'Smartwatch', 'built in LTE, quadband, bluetooth, rechargeable lithium ion battery, smell like new', '2021-01-01', '2021-10-30', '234', '1', 'https://unsplash.com/photos/gtQddXwuS18?modal=%7B%22tag%22%3A%22CreditBadge%22%2C%22value%22%3A%7B%22userId%22%3A%22Iq-RxswgvM4%22%7D%7D', 11);

-- --------------------------------------------------------

--
-- Table structure for table `MEMBER`
--

CREATE TABLE `MEMBER` (
  `MemberID` int(11) NOT NULL,
  `Fname` varchar(255) NOT NULL,
  `Lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` char(2) NOT NULL,
  `postCode` char(6) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MEMBER`
--

INSERT INTO `MEMBER` (`MemberID`, `Fname`, `Lname`, `email`, `street`, `city`, `province`, `postCode`, `password`) VALUES
(1, 'Philip', 'Loren', 'philiploren@hotmail.com', '777 Brockton Avenue', 'Vancouver', 'BC', 'V5H1S0', '098f6bcd4621d373cade4e832627b4f6'),
(2, 'Andrew', 'Barnhart', 'andrewbarnhart@hotmail.com', '30 Memorial Drive', 'Vancouver', 'BC', 'V5H1S1', '098f6bcd4621d373cade4e832627b4f6'),
(3, 'Cassaundra', 'Evens', 'cassaundraevens@hotmail.com', '250 Hartford Avenue', 'Vancouver', 'BC', 'V5H1S2', '098f6bcd4621d373cade4e832627b4f6'),
(4, 'Brigette', 'Schlicher', 'brigetteshlicher@hotmail.com', '700 Oak Street', 'Vancouver', 'BC', 'V5H1S3', '098f6bcd4621d373cade4e832627b4f6'),
(5, 'Tommy', 'Knickerbocker', 'tommyknickerbocker@hotmail.com', '664 Parkhurst Rd', 'Vancouver', 'BC', 'V5H1S4', '098f6bcd4621d373cade4e832627b4f6'),
(6, 'Joellen', 'Kropf', 'joellenkropfn@hotmail.com', '591 Memorial Dr', 'Vancouver', 'BC', 'V5H1S5', '098f6bcd4621d373cade4e832627b4f6'),
(7, 'Allene', 'Spence', 'allenespence@hotmail.com', '55 Brooksby Village Way', 'Vancouver', 'BC', 'V5H1S6', '098f6bcd4621d373cade4e832627b4f6'),
(8, 'Morris', 'Stromberg', 'morrisstromberg@hotmail.com', '137 Teaticket Hwy', 'Vancouver', 'BC', 'V5H1S7', '098f6bcd4621d373cade4e832627b4f6'),
(9, 'Hershel', 'Bannon', 'hershelbannon@hotmail.com', '42 Fairhaven Commons Way', 'Vancouver', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(10, 'Erlinda', 'Jenks', 'erlindajenks@hotmail.com', '42 Fairhaven Commons Way', 'Vancouver', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(11, 'Rocco', 'Laforge', 'roccolaforge@hotmail.com', '30 Fairhaven Commons Way', 'Vancouver', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(12, 'Joellen', 'Kropf', 'joellenkropf@hotmail.com', '42 Brooksby Village Way', 'Vancouver', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(13, 'Cristen', 'Cadogan', 'cristencadogan@hotmail.com', '120 Memorial Dr.', 'Richmond', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(14, 'Celesta', 'Godard', 'celestagodard@hotmail.com', '240 Oak Street', 'Vancouver', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(15, 'Lucius', 'Rumore', 'luciusrumore@hotmail.com', '12 Brockton Avenue', 'Vancouver', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(16, 'Willetta', 'Maltese', 'willetamaltese@hotmail.com', '466 Parkhurst Rd', 'Vancouver', 'BC', 'V5H1S9', '098f6bcd4621d373cade4e832627b4f6'),
(17, 'Brian', 'Langara', 'brian@langara.com', '100 West 49th Avenue', 'Vancouver', 'BC', 'V5Y2Z6', 'b272bda9bf0c1cdcba614b5ed99c4d62'),
(18, 'Joseph', 'Keers', 'josephkeers@hotmail.com', '360-5951 No. 3 Road', 'Richmond', 'BC', 'V6X2E3', '098f6bcd4621d373cade4e832627b4f6'),
(23, 'Anthony', 'Throover', 'anthonythroover@hotmail.com', '525 Confederation Bldg', 'Ottawa', 'ON', 'K1A0A6', '098f6bcd4621d373cade4e832627b4f6');

-- --------------------------------------------------------

--
-- Table structure for table `MEMBER_COMENTS_ON_WONTRANSACTION`
--

CREATE TABLE `MEMBER_COMENTS_ON_WONTRANSACTION` (
  `TransactID` int(11) NOT NULL,
  `MemberID` int(11) NOT NULL,
  `Feedback` varchar(255) DEFAULT NULL,
  `Rate` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MEMBER_COMENTS_ON_WONTRANSACTION`
--

INSERT INTO `MEMBER_COMENTS_ON_WONTRANSACTION` (`TransactID`, `MemberID`, `Feedback`, `Rate`) VALUES
(1, 12, 'I hope you enjoy your new acquisition. All the best. Thanks', 5),
(1, 15, 'Great deal! It was a pleasure make business with you. Thanks', 5),
(2, 6, 'You should pay attention on read. It was all there in description. Better luck next time.', 2),
(2, 13, 'Ok, it was not exactly as described but no worries!', 3),
(3, 4, 'The picture did not show all scratches on it. Very disappointed!', 1),
(3, 7, 'The scratches you complain do not affect the normal equipment function. Sorry about that.', 2);

-- --------------------------------------------------------

--
-- Table structure for table `WON_TRANSACTION`
--

CREATE TABLE `WON_TRANSACTION` (
  `TransactID` int(11) NOT NULL,
  `MemberID` int(11) DEFAULT NULL,
  `itemID` int(11) DEFAULT NULL,
  `BidID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `WON_TRANSACTION`
--

INSERT INTO `WON_TRANSACTION` (`TransactID`, `MemberID`, `itemID`, `BidID`) VALUES
(1, 15, 15, 11),
(2, 13, 12, 12),
(3, 4, 13, 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `BID`
--
ALTER TABLE `BID`
  ADD PRIMARY KEY (`BidID`,`MemberID`,`itemID`),
  ADD KEY `MemberID` (`MemberID`),
  ADD KEY `itemID` (`itemID`);

--
-- Indexes for table `ITEM`
--
ALTER TABLE `ITEM`
  ADD PRIMARY KEY (`itemID`),
  ADD KEY `MemberID` (`MemberID`);

--
-- Indexes for table `MEMBER`
--
ALTER TABLE `MEMBER`
  ADD PRIMARY KEY (`MemberID`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `MEMBER_COMENTS_ON_WONTRANSACTION`
--
ALTER TABLE `MEMBER_COMENTS_ON_WONTRANSACTION`
  ADD PRIMARY KEY (`TransactID`,`MemberID`),
  ADD KEY `MemberID` (`MemberID`);

--
-- Indexes for table `WON_TRANSACTION`
--
ALTER TABLE `WON_TRANSACTION`
  ADD PRIMARY KEY (`TransactID`),
  ADD KEY `MemberID` (`MemberID`),
  ADD KEY `itemID` (`itemID`),
  ADD KEY `BidID` (`BidID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `BID`
--
ALTER TABLE `BID`
  MODIFY `BidID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `ITEM`
--
ALTER TABLE `ITEM`
  MODIFY `itemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `MEMBER`
--
ALTER TABLE `MEMBER`
  MODIFY `MemberID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `WON_TRANSACTION`
--
ALTER TABLE `WON_TRANSACTION`
  MODIFY `TransactID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `BID`
--
ALTER TABLE `BID`
  ADD CONSTRAINT `BID_ibfk_1` FOREIGN KEY (`MemberID`) REFERENCES `MEMBER` (`MemberID`),
  ADD CONSTRAINT `BID_ibfk_2` FOREIGN KEY (`itemID`) REFERENCES `ITEM` (`itemID`);

--
-- Constraints for table `ITEM`
--
ALTER TABLE `ITEM`
  ADD CONSTRAINT `ITEM_ibfk_1` FOREIGN KEY (`MemberID`) REFERENCES `MEMBER` (`MemberID`);

--
-- Constraints for table `MEMBER_COMENTS_ON_WONTRANSACTION`
--
ALTER TABLE `MEMBER_COMENTS_ON_WONTRANSACTION`
  ADD CONSTRAINT `MEMBER_COMENTS_ON_WONTRANSACTION_ibfk_1` FOREIGN KEY (`MemberID`) REFERENCES `MEMBER` (`MemberID`),
  ADD CONSTRAINT `MEMBER_COMENTS_ON_WONTRANSACTION_ibfk_2` FOREIGN KEY (`TransactID`) REFERENCES `WON_TRANSACTION` (`TransactID`);

--
-- Constraints for table `WON_TRANSACTION`
--
ALTER TABLE `WON_TRANSACTION`
  ADD CONSTRAINT `WON_TRANSACTION_ibfk_1` FOREIGN KEY (`MemberID`) REFERENCES `MEMBER` (`MemberID`),
  ADD CONSTRAINT `WON_TRANSACTION_ibfk_2` FOREIGN KEY (`itemID`) REFERENCES `ITEM` (`itemID`),
  ADD CONSTRAINT `WON_TRANSACTION_ibfk_3` FOREIGN KEY (`BidID`) REFERENCES `BID` (`BidID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

```

### 4. Update the db.php configuration file

Replace the heroku connection information inside function `dbConnect()` with the following:

```
      $server = "localhost";
      $user = "Auction";
      $pwd = "<the password you've chosen>";
      $db = "Auction";
```

### 5 Start `MySQL` and the `XAMPP` server

```
go to localhost:<XAMPP running port> to see the application running
```

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
