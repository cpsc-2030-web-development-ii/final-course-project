var tbodyBidModal=document.getElementById("tbodyBidModal");
var tbodyTab;


window.addEventListener('load', function () {
  // activating bootstrap tooltips
  $('[data-toggle="tooltip"]').tooltip();

  // trying to catch the first bid of the table inside the modal  
      $('#modalBid').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var recipient = button.data('whatever') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
    
    //var tdFirst=document.querySelector("#tbodyBidModal > tr:nth-child(1) > td:nth-child(2)");
    //$('#bidAmount').val(tdFirst.textContent);
      
     // modal.find('.modal-title').text('Dentro do modal');
      //modal.find('.modal-body input').val(recipient)
    })
});


function reqListener () {
    this.response.forEach( function( record ){
    addBidItem( record );
    });
  }

function bidItem (ItemID){
  //var tbodyTabID="tbodyTab"+ItemID.toString();
  //tbodyTab = document.getElementById(tbodyTabID);
  
  $("#tbodyBidModal").find("tr").remove(); //removing previous results in order
  var oReq = new XMLHttpRequest();         // to be able to use the same modal
  oReq.addEventListener("load", reqListener);
  oReq.responseType = "json";
  oReq.open("GET", "bids.php?ID="+ItemID);
  oReq.send();
  //var tdFirst=document.querySelector("#tbodyBidModal > tr:nth-child(1) > td:nth-child(2)");
  //var tdFirst=$("#tbodyBidModal > tr:nth-child(1) > td:nth-child(1)").text();
  //console.log("first" +tdFirst);
  //$("#bidAmount").val(tdFirst);
  $('#modalBid').modal({show:true});
  
  
  
  
    console.log('item id: '+ItemID);
}

function addBidItem(record){
  var tableRow = document.createElement('tr');
  var rDt = document.createElement('td');
  var rBid = document.createElement('td');
  
  rDt.textContent=record.BTime;
  rBid.textContent=record.BPrice;
  tableRow.appendChild(rDt);
  tableRow.appendChild(rBid);
  tbodyBidModal.appendChild(tableRow);
}