<?php
    session_start();
    
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    
    if ($_SESSION["loggedin"]){
        $logFlag="Logout";
        $linkLogFlag="'logout.php'";
        $menuNavFlag="'nav-link'";
        $userName=$_SESSION['userName'];
    } else{
        $logFlag="Log in";
        $linkLogFlag="'login.php'";
        $menuNavFlag="'nav-link disabled'";
        $userName='';
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="auctions sell buy offers">
  <meta name="author" content="Pablo Soares">

  <title>Open Market</title>

   <!--Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  
  <!--font awesome -->
	<link href="css/all.css" rel="stylesheet">

  
  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">Open Market</a>
      <span class="badge badge-dark"><?php !empty($userName) ? print "Hi, $userName"  : "" ; ?></span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="feedback.php">Feedbacks</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sources.php">Sources</a>
          </li>
          <li class="nav-item">
            <a class=<?php echo $menuNavFlag ?> href="account.php">Account</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href=<?php echo $linkLogFlag ?>><?php echo $logFlag ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="documentation.php">About</a>
          </li>
					<li class="nav-item">
      	      <a class="nav-link" href="https://gitlab.com/cpsc-2030-web-development-ii/final-course-project.git"> GitLab
      	         <img src="img/GitLab_Logo.svg" width="30" height="30" alt="gitlab">
            </a>
      	  </li>
        </ul>
      </div>
    </div>
  </nav>
<header>
</header>
   <div id="course" class="container">  
    <h1>Sources</h1>
    <p> All photos utilized in the project came from unsplash and pexels.</p>
    <p>Every time one is used, the correspondent link is also printed followed by the  <i class='far fa-copyright'></i> copyright symbol. </p>
    <p> It is just to click the copyright symbol to be redirected to the correspondent page, where it is possible to check the license.</p>
    <p>I also utilized the free part of font-awesome library available in https://fontawesome.com/.</p>
     <!-- Footer -->
  </div>


  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Open Market 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

</body>
</html>